package com.rocketmail;

import com.google.gson.Gson;
import com.rocketmail.json.OutlookEmailJson;
import com.rocketmail.model.OutlookEmails;
import com.rocketmail.service.OutlookService;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@Service
public class DeliverJob {

    private Logger logger = LoggerFactory.getLogger(DeliverJob.class);

    @Autowired
    private OutlookService outlookService;

    @Scheduled(initialDelay = 1000, fixedDelay = 1000 * 60)
    public void delivered(){
        Random random = new Random();
        List<OutlookEmails> emails = outlookService.getMessagesPending();
        //logger.info("Get email to delivers , how many : "+emails.size());
        for (OutlookEmails email : emails) {
            try {

                outlookService.sendEmail(email);
                //logger.info("Mark as processed");
                outlookService.markAsDelivered(email);
                TimeUnit.MILLISECONDS.sleep(4000 + random.nextInt(1000));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
