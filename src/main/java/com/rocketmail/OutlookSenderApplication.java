package com.rocketmail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication(exclude = HibernateJpaAutoConfiguration.class)
@EnableAsync
@EnableScheduling
public class OutlookSenderApplication {

    public static void main(String[] args) {
        SpringApplication.run(OutlookSenderApplication.class, args);
    }

}
