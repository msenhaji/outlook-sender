package com.rocketmail.json;


import com.rocketmail.model.UserTemplates;

public class OutlookEmailJson {

    private UserTemplates template;
    private String from;
    private String name;
    private String token;
    private String to;
    private String subject;

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public OutlookEmailJson(UserTemplates template,
                            String from,
                            String name,
                            String token,
                            String subject,
                            String to) {
        this.template = template;
        this.from = from;
        this.name = name;
        this.token = token;
        this.to = to;
        this.subject = subject;
    }

    public UserTemplates getTemplate() {
        return template;
    }

    public void setTemplate(UserTemplates template) {
        this.template = template;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
