package com.rocketmail.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "outlook_emails")
public class OutlookEmails {
    private Long id;
    private String uuid;
    private String jsonObject;
    private int status;
    private Long createdOn;
    private Long processedOn;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "uuid")
    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Basic
    @Column(name = "jsonObject")
    public String getJsonObject() {
        return jsonObject;
    }

    public void setJsonObject(String jsonObject) {
        this.jsonObject = jsonObject;
    }

    @Basic
    @Column(name = "status")
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Basic
    @Column(name = "createdOn")
    public Long getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Long createdOn) {
        this.createdOn = createdOn;
    }

    @Basic
    @Column(name = "processedOn")
    public Long getProcessedOn() {
        return processedOn;
    }

    public void setProcessedOn(Long processedOn) {
        this.processedOn = processedOn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OutlookEmails that = (OutlookEmails) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(uuid, that.uuid) &&
                Objects.equals(jsonObject, that.jsonObject) &&
                Objects.equals(status, that.status) &&
                Objects.equals(createdOn, that.createdOn) &&
                Objects.equals(processedOn, that.processedOn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, uuid, jsonObject, status, createdOn, processedOn);
    }
}
