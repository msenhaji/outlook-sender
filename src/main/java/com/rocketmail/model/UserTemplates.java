package com.rocketmail.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "user_templates")
public class UserTemplates {
    private int id;
    private String name;
    private String content;
    private byte shared;
    private byte deleted;
    private Long createdOn;
    private Long updateOn;
    private String type;
    private String mjmlContent;
    private String image;
    private String ampContent;
    private String uuid;
    private String textContent;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "content")
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Basic
    @Column(name = "shared")
    public byte getShared() {
        return shared;
    }

    public void setShared(byte shared) {
        this.shared = shared;
    }

    @Basic
    @Column(name = "deleted")
    public byte getDeleted() {
        return deleted;
    }

    public void setDeleted(byte deleted) {
        this.deleted = deleted;
    }

    @Basic
    @Column(name = "createdOn")
    public Long getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Long createdOn) {
        this.createdOn = createdOn;
    }

    @Basic
    @Column(name = "updateOn")
    public Long getUpdateOn() {
        return updateOn;
    }

    public void setUpdateOn(Long updateOn) {
        this.updateOn = updateOn;
    }

    @Basic
    @Column(name = "type")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Basic
    @Column(name = "mjml_content")
    public String getMjmlContent() {
        return mjmlContent;
    }

    public void setMjmlContent(String mjmlContent) {
        this.mjmlContent = mjmlContent;
    }

    @Basic
    @Column(name = "image")
    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Basic
    @Column(name = "amp_content")
    public String getAmpContent() {
        return ampContent;
    }

    public void setAmpContent(String ampContent) {
        this.ampContent = ampContent;
    }

    @Basic
    @Column(name = "uuid")
    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
       UserTemplates that = (UserTemplates) o;
        return id == that.id &&
                shared == that.shared &&
                deleted == that.deleted &&
                Objects.equals(name, that.name) &&
                Objects.equals(content, that.content) &&
                Objects.equals(createdOn, that.createdOn) &&
                Objects.equals(updateOn, that.updateOn) &&
                Objects.equals(type, that.type) &&
                Objects.equals(mjmlContent, that.mjmlContent) &&
                Objects.equals(image, that.image) &&
                Objects.equals(ampContent, that.ampContent) &&
                Objects.equals(uuid, that.uuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, content, shared, deleted, createdOn, updateOn, type, mjmlContent, image, ampContent, uuid);
    }

    @Transient
    public String getTextContent() {
        return textContent;
    }

    public void setTextContent(String txt){
        this.textContent = txt;
    }
}
