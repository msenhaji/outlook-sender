package com.rocketmail.service;

import com.rocketmail.util.MySMTPMessage;
import com.sun.mail.smtp.SMTPMessage;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.IOException;
import java.util.Date;
import java.util.GregorianCalendar;

public class MimeMessageBuilder {


    public static MimeMessage build(MimeMessage mimeMessage,
                                    String html,
                                    String text,
                                    String amp,
                                    String trackerId,
                                    String fromAddress,
                                    String to,
                                    String senderName,
                                    String subject,
                                    String language) throws IOException, MessagingException {


        InternetAddress from = new InternetAddress(fromAddress);
        from.setPersonal(senderName);
        mimeMessage.setFrom(from);
        mimeMessage.setReplyTo(new Address[]{from});


        MimeMultipart contentMultiPart = new MimeMultipart("alternative");

        MimeBodyPart txtPart = new MimeBodyPart();
        txtPart.setText(text, "UTF-8");
        contentMultiPart.addBodyPart(txtPart);

        if(StringUtils.isNotBlank(amp)){
            MimeBodyPart ampPart = new MimeBodyPart();
            ampPart.setContent(amp, "text/x-amp-html");
            contentMultiPart.addBodyPart(ampPart);
        }

        MimeBodyPart htmlPart = new MimeBodyPart();
        htmlPart.setContent(html, "text/html;charset=\"UTF-8\"");
        contentMultiPart.addBodyPart(htmlPart);

        mimeMessage.setContent(contentMultiPart);

        mimeMessage.setContentLanguage(new String[]{language != null ? language : "en"});

        mimeMessage.setSubject(subject);

        InternetAddress toAddress = new InternetAddress( to);
        mimeMessage.addRecipient(Message.RecipientType.TO, toAddress);
        mimeMessage.setSentDate(new Date());

        SMTPMessage smtpMessage = new MySMTPMessage(mimeMessage, fromAddress);
        if(StringUtils.isNotBlank(trackerId)){
            smtpMessage.setEnvelopeFrom("tbounces="+trackerId+"@rocketmail.lu");
        }else {
            smtpMessage.setEnvelopeFrom(fromAddress);
        }

        return smtpMessage;
    }
}
