package com.rocketmail.service;

import com.detectlanguage.DetectLanguage;
import com.detectlanguage.errors.APIError;
import com.google.gson.Gson;
import com.rocketmail.json.OutlookEmailJson;
import com.rocketmail.model.OutlookEmails;
import com.rocketmail.model.UserTemplates;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.List;
import java.util.Properties;

@Service
public class OutlookService {

    @Autowired
    private SessionFactory sessionFactory;


    private Logger logger = LoggerFactory.getLogger(OutlookService.class);

    @Transactional(readOnly = true)
    public List<OutlookEmails> getMessagesPending() {
        Query query = sessionFactory.getCurrentSession().createQuery("from OutlookEmails where status = 0 order by createdOn asc");
        query.setMaxResults(10);
        return query.list();
    }

    public void sendEmail(OutlookEmails email) throws Exception {
        String host = "smtp.office365.com";
        String port = "587";
        String address = "valentina@rocketmail.lu";
        String pass = "A3X9LNUn2";
        Properties props = new Properties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.quitwait", "false");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", port);

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(address, pass);
                    }
                });
        Gson gson = new Gson();
        String jsonObject = email.getJsonObject();
        OutlookEmailJson json = gson.fromJson(jsonObject, OutlookEmailJson.class);

        //logger.info("Send email subject : "+json.getSubject());

        UserTemplates template = json.getTemplate();
        MimeMessage message = new MimeMessage(session);
        MimeMessage mime = MimeMessageBuilder.build(message,
                template.getContent(),
                template.getTextContent(),
                template.getAmpContent(),
                json.getToken(),
                json.getFrom(),
                json.getTo(),
                json.getName(),
                json.getSubject(),
                detectLangage(template.getContent()));

       // logger.info("Connexion smtp");
        Transport transport = session.getTransport("smtp");
        transport.connect(host, address, pass);

       // logger.info("Send smtp");
        transport.sendMessage(mime, message.getAllRecipients());
       // logger.info("close smtp");
        transport.close();

    }

    public  String detectLangage(String text){

        DetectLanguage.apiKey = "8ec9fedf728b0e1a864e877efa58e0cb";

        try {
            return DetectLanguage.simpleDetect(text);
        } catch (APIError apiError) {
            apiError.printStackTrace();
            return "fr";
            //throw new IOException("Error in API detect lang", apiError);
        }
    }

    @Transactional
    public void markAsDelivered(OutlookEmails email) {
        email.setStatus(1);
        email.setProcessedOn(new Date().getTime());
        sessionFactory.getCurrentSession().update(email);
    }
}
