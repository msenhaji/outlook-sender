package com.rocketmail.util;

import com.sun.mail.smtp.SMTPMessage;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.UUID;

public class MySMTPMessage extends SMTPMessage {

    private String fromEmail;

    public MySMTPMessage(MimeMessage source, String fromEmail) throws MessagingException {
        super(source);
        this.fromEmail = fromEmail;
        updateMessageID();
    }

    @Override
    protected void updateMessageID() throws MessagingException {
        setHeader("Message-ID", "<"+UUID.randomUUID().toString() + "@" + fromEmail.split("@")[1] + ">");
    }
}
